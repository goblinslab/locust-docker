# locust-docker

Locust in container!

Source: https://medium.com/locust-io-experiments/locust-io-experiments-running-in-docker-cae3c7f9386e

## Build

docker build -t goblinslab/locust:1.0 .

## Run standalone

```console
docker run --rm --name standalone --hostname standalone \
  -e ATTACKED_HOST=http://mytargettobench:80 \
  -v /opt/locust_script.py:/opt/locust_script.py \
  -e LOCUST_FILE=/opt/locust_script.py \
  -p 8089:8089 -d \
  goblinslab/locust:1.0
```

## Run distributed (master/slave)

```console
docker run --name master --hostname master \
  -p 8089:8089 -p 5557:5557 -p 5558:5558 \
  -v /home/julien/git/front-stack/terraform/05-bench/locust_script.py:/opt/locust_script.py \
  -e LOCUST_FILE=/opt/locust_script.py \
  -e ATTACKED_HOST=http://mytargettobench:80 \
  -e LOCUST_MODE=master \
  --rm -d goblinslab/locust:1.0
```

```console
 docker run --name slave \
  --link master --env NO_PROXY=master \
  -v /home/julien/git/front-stack/terraform/05-bench/locust:/locust \
  -e LOCUST_FILE=/opt/locust_script.py \
  -e ATTACKED_HOST=http://mytargettobench:80 \
  -e LOCUST_MODE=slave \
  -e LOCUST_MASTER=master \
  --rm -d goblinslab/locust:1.0
```
