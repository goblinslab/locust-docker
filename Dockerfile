# From https://medium.com/locust-io-experiments/locust-io-experiments-running-in-docker-cae3c7f9386e
FROM python:3.7.3-alpine3.9

COPY docker-entrypoint.sh /

RUN    apk update \
    && apk --no-cache add --virtual=.build-dep build-base libzmq musl-dev python3 python3-dev zeromq-dev \
    && pip3 install --no-cache-dir pyzmq \
    && pip3 install --no-cache-dir locustio==0.11.0 \
    # reduce image size by cleaning up the build packages
    #&& apk del .build-dep build-base musl-dev python3-dev \
    && chmod +x /docker-entrypoint.sh

RUN  mkdir /locust
#COPY locustfile.py /locust/
WORKDIR /locust

EXPOSE 8089 5557 5558

ENTRYPOINT ["/docker-entrypoint.sh"]